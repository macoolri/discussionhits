﻿using Discussion.Data;
using Discussion.Models;
using Discussion.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace Discussion.Services
{
    public interface IDiscussService
    {
        Task Create(DiscussCreateViewModel model, ClaimsPrincipal currentUserClaims);
        Task<List<DiscussViewModel>> GetAllDiscussions();
    }

    public class DiscussService : IDiscussService
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        public DiscussService(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task Create(DiscussCreateViewModel model, ClaimsPrincipal currentUserClaims)
        {
            var user = await _userManager.GetUserAsync(currentUserClaims);

            await _context.Discussions.AddAsync(new Discuss
            {
                Name = model.Name,
                Description = model.Description,
                Author = user
            });
            await _context.SaveChangesAsync();
        }

        public async Task<List<DiscussViewModel>> GetAllDiscussions()
        {
            var discussions = await _context.Discussions.Select(x => new DiscussViewModel
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Author = x.Author,
                Comments = x.Comments
            }).ToListAsync();
            return discussions;
        }
    }
}
