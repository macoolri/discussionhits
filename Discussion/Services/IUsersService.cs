﻿using Discussion.Models;
using Discussion.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace Discussion.Services
{
    public interface IUsersService
    {
        Task SignUp(SignUpViewModel model);
        Task SignIn(SignInViewModel model);
        Task Logout();
    }

    public class UsersService : IUsersService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IWebHostEnvironment _environment;
        private static string[] AllowedExtensions { get; set; } = { "jpg", "jpeg", "png" };

        public UsersService(UserManager<User> userManager,
            SignInManager<User> signInManager, IWebHostEnvironment environment)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _environment = environment;
        }

        public async Task SignUp(SignUpViewModel model)
        {
            var fileNameWithPath = await AddFilePath(model.File);
            var user = new User
            {
                Email = model.Email,
                UserName = model.Username,
                Name = model.Name,
                Group = model.Group,
                FilePath = fileNameWithPath
            };
            var result = await _userManager.CreateAsync(user, model.Password); 
            if (result.Succeeded) 
            {
                await _signInManager.SignInAsync(user, false);
                return;
            }
            var errors = string.Join(", ", result.Errors.Select(x => x.Description));
            throw new ArgumentException(errors);

        }

        public async Task SignIn(SignInViewModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                throw new KeyNotFoundException($"Пользователя с email = {model.Email} не существует");
            }

            var claims = new List<Claim>
            {
                new ("Name", user.Name),
                new ("Id", user.Id.ToString()),
                new (ClaimTypes.NameIdentifier, user.Id.ToString())
            };


            var authProperties = new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddDays(2),
                IsPersistent = true
            };

            await _signInManager.SignInWithClaimsAsync(user, authProperties, claims);
        }

        public async Task Logout()
        {
            await _signInManager.SignOutAsync();
        }

        private async Task<string> AddFilePath(IFormFile file)
        {
            var isFileAttached = file != null;
            var fileNameWithPath = string.Empty;
            if (isFileAttached)
            {
                var extension = Path.GetExtension(file.FileName).Replace(".", "");
                if (!AllowedExtensions.Contains(extension))
                {
                    throw new ArgumentException("Attached file has not supported extension");
                }
                fileNameWithPath = $"files/{Guid.NewGuid()}-{file.FileName}";
                using (var fs = new FileStream(Path.Combine(_environment.WebRootPath, fileNameWithPath), FileMode.Create))
                {
                    await file.CopyToAsync(fs);
                }
            }

            return fileNameWithPath;
        }
    }
}
