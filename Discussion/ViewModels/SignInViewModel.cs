﻿using System.ComponentModel.DataAnnotations;

namespace Discussion.ViewModels
{
    public class SignInViewModel
    {
        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Логин или Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
    }
}
