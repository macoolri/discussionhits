﻿
namespace Discussion.ViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<DiscussViewModel> Discussions { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
