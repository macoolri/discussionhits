﻿using System.ComponentModel.DataAnnotations;

namespace Discussion.ViewModels
{
    public class DiscussCreateViewModel
    {
        [Required(ErrorMessage = "Необходимо заполнить")]
        [Display(Name = "Заголовок")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Необходимо заполнить")]
        [Display(Name = "Описание")]
        public string Description { get; set; }
    }
}
