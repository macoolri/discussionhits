﻿using Discussion.Models;

namespace Discussion.ViewModels
{
    public class DiscussViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public User Author { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }


}
