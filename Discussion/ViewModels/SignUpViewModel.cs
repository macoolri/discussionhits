﻿using System.ComponentModel.DataAnnotations;

namespace Discussion.ViewModels
{
    public class SignUpViewModel
    {
        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Имя Фамилия")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Номер группы")]
        public int Group { get; set; }

        [EmailAddress(ErrorMessage = "Необходимо указать корректный email")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Логин")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [Display(Name = "Подтвердить пароль")]
        public string PasswordConfirm { get; set; }

        public IFormFile? File { get; set; }
    }
}
