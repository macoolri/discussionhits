﻿using Microsoft.AspNetCore.Identity;

namespace Discussion.Models
{
    public class User : IdentityUser
    {
        public string Name { get; set; }
        public int Group { get; set; }
        public string FilePath { get; set; }
        public ICollection<Discuss> Discussions { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
