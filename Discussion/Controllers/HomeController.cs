﻿using Discussion.Models;
using Discussion.Services;
using Discussion.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Discussion.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IDiscussService _discussService;

        public HomeController(ILogger<HomeController> logger, IDiscussService discussService)
        {
            _logger = logger;
            _discussService = discussService;
        }

        public async Task<IActionResult> Index(int page = 1)
        {
            int pageSize = 12;

            var discs = await _discussService.GetAllDiscussions();
            var count = discs.Count;
            var items = discs.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                Discussions = items
            };
            return View(viewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}