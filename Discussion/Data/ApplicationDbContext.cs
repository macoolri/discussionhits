﻿using Discussion.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Discussion.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
           : base(options)
        {
        }
        public override DbSet<User> Users { get; set; }
        public DbSet<Discuss> Discussions { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>(x =>
            {
                x.ToTable("Users");
                x.HasKey(x => x.Id);
                x.HasMany(x => x.Discussions)
                    .WithOne(x => x.Author);
                x.HasMany(x => x.Comments)
                    .WithOne(x => x.Author);
            });

            builder.Entity<Discuss>(x =>
            {
                x.ToTable("Discussions");
                x.HasKey(x => x.Id);
            });

            builder.Entity<Comment>(x =>
            {
                x.ToTable("Comments");
                x.HasKey(x => x.Id);
            });
        }

    }
}
